const gulp = require('gulp');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');

gulp.task('babel_eslint', () =>
    gulp.src('src/js/*.js')
    /*.pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())*/
    .pipe(babel({
        presets: ['env']
    }))
    .pipe(gulp.dest('dist/js'))
 );
gulp.task('default', ['babel_eslint']);

gulp.task('es6', () =>
    gulp.src('src/js/*.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist/js'))
);

gulp.task('watch', ['es6'], () =>
  // run watch
    gulp.watch('src/js/*.js', ['es6'])
);
