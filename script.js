'use strict';
var teacher = ['Иванов С.В', 'Петров Ф.М', 'Васечкин Д.Ю', 'Варнава Ю.В', 'Альментева А.В'];
/*var student = ['Сидоров', 'Явлинский', 'Адотин', 'Суботин', 'Миронова', 'Воробьев', 'Анафьева',
    'Стромин', 'Тюленин', 'Заморин', 'Сергеев', 'Федоров', 'Зельдин', 'Щукин', 'Улюкаев'];
var subject = ['Алгебра', 'Экономика', 'Основы права', 'Физика', 'Геометрия', 'Химия', 'Философия',
    'Мат анализ', 'Программирование', 'Физкультура'];*/

var subTeach = {
    'Иванов С.В': ['Physical culture'],
    'Петров Ф.М': ['Economy', 'Fundamentals of law', 'Philosophy'],
    'Васечкин Д.Ю': ['Chemistry', 'Programming'],
    'Варнава Ю.В': ['Geometry', 'Algebra'],
    'Альментева А.В': ['Physics', 'Math analysis']
};

var groups = {
    group1: ['Сидоров С', 'Явлинский Я', 'Адотин А'],
    group2: ['Суботин С', 'Миронова М', 'Воробьев В'],
    group3: ['Анафьева А', 'Стромин С', 'Тюленин Т'],
    group4: ['Заморин З', 'Сергеев С', 'Федоров Ф'],
    group5: ['Зельдин З', 'Щукин К', 'Улюкаев К']
};

var teachGroup = {
    'Иванов С.В': 'group1',
    'Петров Ф.М': 'group2',
    'Васечкин Д.Ю': 'group3',
    'Варнава Ю.В': 'group4',
    'Альментева А.В': 'group5'
};

var subStudent = {
    'Сидоров С': {'Physical culture': 4, Economy: 3, Geometry: 5},
    'Явлинский Я': {'Physical culture': 4, Programming: 5, 'Math analysis': 5},
    'Адотин А': {'Physical culture': 4, Geometry: 5, Chemistry: 4},
    'Суботин С': {Economy: 2, 'Fundamentals of law': 5, Philosophy: 3},
    'Миронова М': {Economy: 5, 'Fundamentals of law': 4, Philosophy: 5},
    'Воробьев В': {Economy: 4, 'Fundamentals of law': 5, Philosophy: 4},
    'Анафьева А': {Chemistry: 4, Programming: 5, 'Math analysis': 5},
    'Стромин С': {Chemistry: 4, Programming: 3, 'Fundamentals of law': 5},
    'Тюленин Т': {Chemistry: 5, Programming: 5, Physics: 5},
    'Заморин З': {Geometry: 4, Algebra: 5, Physics: 5},
    'Сергеев С': {Geometry: 5, Algebra: 2, Programming: 4},
    'Федоров Ф': {Geometry: 4, Algebra: 3, Programming: 4},
    'Зельдин З': {Physics: 5, 'Math analysis': 4, 'Fundamentals of law': 5},
    'Щукин К': {Physics: 4, 'Math analysis': 5, 'Physical culture': 2},
    'Улюкаев К': {Physics: 3, 'Math analysis': 4, Chemistry: 5}
};

var midleValueStudents = {};
for (var name in subStudent) {
    var sum = 0;
    var counter = 0;
    for (var key in subStudent[name]) {
        sum += subStudent[name][key];
        counter += 1;
    }
    midleValueStudents[name] = sum / counter;
}

var teacheValueStudents = [];
var groupName = [];
for (var val in subTeach) {
    for (var nameSt in subStudent) {
        var sume = false;
        for (var unik in subStudent[nameSt]) {
            if (subTeach[val].indexOf(unik) >= 0) {
                sume = true;
            }
        }
        if (sume) {
            teacheValueStudents.push(nameSt);
        }
    }
    groupName.push(teacheValueStudents);
    teacheValueStudents = [];
}

var bestStudentToTeache = {};
groupName.forEach(function (item, i) {
    var studElem = item.map(function (elem) {
        var intermediateName = 0;
        var numerik = 0;
        var counter = 0;
        var bestStudent = '';
        for (var prom in subStudent[elem]) {
            if (subTeach[teacher[i]].indexOf(prom) >= 0) {
                numerik += subStudent[elem][prom];
                counter += 1;
            }
            intermediateName = Math.max(intermediateName, numerik / counter);
            if (intermediateName === numerik / counter) {
                bestStudent = elem;
            }
        }
        return bestStudent;
    });
    function compareNumeric(a, b) {
        if (a > b) {
            return -1;
        }
        if (a < b) {
            return 1;
        }
    }
    studElem.sort(compareNumeric);
    var studentBest = studElem.slice(0, Math.ceil(studElem.length * 0.05));
    bestStudentToTeache[teacher[i]] = studentBest;
    /*if ((studElem.length * 0.05) < 1) {
        bestStudentToTeache[teacher[i]] = (studElem[0]);
    } else {
        bestStudentToTeache[teacher[i]] = (studElem);
    }*/
});
// Лучший студент по придметам у каждого преподавателя
console.table(bestStudentToTeache);

var bestTeache = {};
groupName.forEach(function (item, i) {
    var TeacheElem = item.map(function (elem) {
        var intermediateName = 0;
        var numerebl = 0;
        var bestT = '';
        for (var promS in subStudent[elem]) {
            if (subTeach[teacher[i]].indexOf(promS) >= 0) {
                numerebl += subStudent[elem][promS];
            }
            intermediateName = Math.max(intermediateName, numerebl);
            if (intermediateName === numerebl) {
                bestT = teacher[i];
            }
        }
        return bestT;
    });
    bestTeache['Лучший учитель:'] = (TeacheElem[0]);
});
//Лучший учитель
console.table(bestTeache);

var max = 0;
var maxName = '';
var maxNameTab = {};
for (var nameVal in midleValueStudents) {
    if (max < midleValueStudents[nameVal]) {
        max = midleValueStudents[nameVal];
        maxName = nameVal;
    }
}
maxNameTab['Лучший ученик:'] = maxName;

var bestStGrups = {};
for (var j in groups) {
    var lid = 0;
    var lidStudent = '';
    for (var i = 0; i < groups[j].length; i++) {
        lid = Math.max(lid, midleValueStudents[groups[j][i]]);
        if (lid === midleValueStudents[groups[j][i]]) {
            lidStudent = groups[j][i];
        }
    }
    bestStGrups[groups[j]] = lidStudent;
}
//Лучший в группе
console.table(bestStGrups);
//Лучший ученик
console.table(maxNameTab);
//Группа
console.table(subStudent);
