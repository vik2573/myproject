'use strict';
var teacher = ['Иванов С.В', 'Петров Ф.М', 'Васечкин Д.Ю', 'Варнава Ю.В', 'Альментева А.В'];
/*var student = ['Сидоров', 'Явлинский', 'Адотин', 'Суботин', 'Миронова', 'Воробьев', 'Анафьева',
    'Стромин', 'Тюленин', 'Заморин', 'Сергеев', 'Федоров', 'Зельдин', 'Щукин', 'Улюкаев'];
var subject = ['Алгебра', 'Экономика', 'Основы права', 'Физика', 'Геометрия', 'Химия', 'Философия',
    'Мат анализ', 'Программирование', 'Физкультура'];*/

var subTeach = {
    'Иванов С.В': ['Physical culture'],
    'Петров Ф.М': ['Economy', 'Fundamentals of law', 'Philosophy'],
    'Васечкин Д.Ю': ['Chemistry', 'Programming'],
    'Варнава Ю.В': ['Geometry', 'Algebra'],
    'Альментева А.В': ['Physics', 'Math analysis']
};

var groups = {
    group1: ['Сидоров С', 'Явлинский Я', 'Адотин А'],
    group2: ['Суботин С', 'Миронова М', 'Воробьев В'],
    group3: ['Анафьева А', 'Стромин С', 'Тюленин Т'],
    group4: ['Заморин З', 'Сергеев С', 'Федоров Ф'],
    group5: ['Зельдин З', 'Щукин К', 'Улюкаев К']
};

var teachGroup = {
    'Иванов С.В': 'group1',
    'Петров Ф.М': 'group2',
    'Васечкин Д.Ю': 'group3',
    'Варнава Ю.В': 'group4',
    'Альментева А.В': 'group5'
};

var subStudent = {
    'Сидоров С': {'Physical culture': 4, Economy: 3, Geometry: 5},
    'Явлинский Я': {'Physical culture': 4, Programming: 5, 'Math analysis': 5},
    'Адотин А': {'Physical culture': 4, Geometry: 5, Chemistry: 4},
    'Суботин С': {Economy: 2, 'Fundamentals of law': 5, Philosophy: 3},
    'Миронова М': {Economy: 5, 'Fundamentals of law': 4, Philosophy: 5},
    'Воробьев В': {Economy: 4, 'Fundamentals of law': 5, Philosophy: 4},
    'Анафьева А': {Chemistry: 4, Programming: 5, 'Math analysis': 5},
    'Стромин С': {Chemistry: 4, Programming: 3, 'Fundamentals of law': 5},
    'Тюленин Т': {Chemistry: 5, Programming: 5, Physics: 5},
    'Заморин З': {Geometry: 4, Algebra: 5, Physics: 5},
    'Сергеев С': {Geometry: 5, Algebra: 2, Programming: 4},
    'Федоров Ф': {Geometry: 4, Algebra: 3, Programming: 4},
    'Зельдин З': {Physics: 5, 'Math analysis': 4, 'Fundamentals of law': 5},
    'Щукин К': {Physics: 4, 'Math analysis': 5, 'Physical culture': 2},
    'Улюкаев К': {Physics: 3, 'Math analysis': 4, Chemistry: 5}
};
//Находим средную оценку по предменам у студента
var midleValueStudents = {};
forIn (subStudent, function(subjects, name) {
    var counter = 0;
    var sum = reduce (subjects, midlGrade);
    midleValueStudents[name] = sum / counter;

    function midlGrade(accum, grade) {
        counter += 1;
        return accum + grade;
    }
});
//Одинаковые предметы у ученика и преподователя
var groupName = [];
var teacheValueStudents = [];
forIn (subTeach, function(discipline) {
    forIn (subStudent, function(value, name) {
       if (some (value, function(a, subjects) {
            return discipline.indexOf(subjects) >= 0;
        }))
        teacheValueStudents.push(name);
    });
    groupName.push(teacheValueStudents);
    teacheValueStudents = [];
});
//console.log(groupName);
//Лучшие 5% учеников у учителя
var bestStudentToTeache = {};
groupName.forEach(function (item, i) {
    var studElem = item.map(function (elem) {
        var intermediateName = 0;
        var numerik = 0;
        var counter = 0;
        var bestStudent = '';
        forIn (subStudent[elem], function(subjects, key) {
            if (subTeach[teacher[i]].indexOf(key) >= 0) {
                numerik += subjects;
                counter += 1;
            }
            intermediateName = Math.max(intermediateName, numerik / counter);
            if (intermediateName === numerik / counter) {
                bestStudent = elem;
            }
        });
        return bestStudent;
    });
    studElem.sort(compareNumeric);
    var studentBest = studElem.slice(0, Math.ceil(studElem.length * 0.05));
    bestStudentToTeache[teacher[i]] = studentBest;
});

function compareNumeric(a, b) {
    if(a > b) {return -1;}
    if(a < b) {return 1;}
}
// Лучший студент по придметам у каждого преподавателя
console.table(bestStudentToTeache);
//Лучший учитель
var bestTeache = {};
groupName.forEach(function (item, i) {
    var TeacheElem = item.map(function (elem) {
        var intermediateName = 0;
        var numerebl = 0;
        var bestT = '';
        forIn (subStudent[elem], function(subjects, promS) {
            if (subTeach[teacher[i]].indexOf(promS) >= 0) {
                numerebl += subStudent[elem][promS];
            }
            intermediateName = Math.max(intermediateName, numerebl);
            if (intermediateName === numerebl) {
                bestT = teacher[i];
            }
        });
        return bestT;
    });
    bestTeache['Лучший учитель:'] = (TeacheElem[0]);
});
console.table(bestTeache);

var max = 0;
var maxName = '';
var maxNameTab = {};
forIn (midleValueStudents, function(subjects, nameVal) {
    if (max < midleValueStudents[nameVal]) {
        max = midleValueStudents[nameVal];
        maxName = nameVal;
    }
});
maxNameTab['Лучший ученик:'] = maxName;

var bestStGrups = {};
forIn (groups, function(subjects, j) {
    var lid = 0;
    var lidStudent = '';
    groups[j].forEach(function (item, i) {
        lid = Math.max(lid, midleValueStudents[groups[j][i]]);
        if (lid === midleValueStudents[groups[j][i]]) {
            lidStudent = groups[j][i];
        }
    });
    bestStGrups[groups[j]] = lidStudent;
});
//Лучший в группе
console.table(bestStGrups);
//Лучший ученик
console.table(maxNameTab);
//Группа
console.table(subStudent);

//преобразовывает цикл for in в функцию
function forIn(object, callback, thisArg) {

    var T, k;

    if (object == null) {
         throw new TypeError(' object is null or not defined');
    }
    var O = Object(object);
    if (typeof callback !== 'function') {
         throw new TypeError(callback + ' is not a function');
    }
    if (arguments.length > 2) {
         T = thisArg;
    }
    for (k in O) {
         var kValue;
             kValue = O[k];
             callback.call(T, kValue, k, O);
    }
}

//схлопывает массив
function reduce(object, callback, init) {

    var k;

    if (object == null) {
         throw new TypeError(' object is null or not defined');
    }
    var O = Object(object);
    if (typeof callback !== 'function') {
         throw new TypeError(callback + ' is not a function');
    }
    var kValue = 0;
    if (arguments.length > 2) {
         kValue = init;
    }
    for (k in O) {
         kValue = callback(kValue, O[k], k, O);
    }
    return kValue;
}
//проверяет есть ло такое значение у другого обьекта
function some(obj, callback/*, thisArg*/) {

    if (obj == null) {
        throw new TypeError('Array.prototype.some called on null or undefined');
    }

    if (typeof callback !== 'function') {
        throw new TypeError();
    }

    var t = Object(obj);

    var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
    for (var i in obj) {
        if (callback.call(thisArg, obj[i], i, t)) {
             return true;
      }
    }

    return false;
}